package com.example.easytrainapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class PUReader extends AppCompatActivity implements View.OnClickListener {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pureader);




        //prendo i riferimenti del bottone

        Button backHome = findViewById(R.id.backHome);
        Button sendButton = findViewById(R.id.invio);


        sendButton.setOnClickListener(this);


        backHome.setClickable(false);
        backHome.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {

        //prendo i riferimenti del puCodeInput
        TextView puCode = findViewById(R.id.puReaderInput);
        Button backHome = findViewById(R.id.backHome);

        switch(view.getId()) {

            case R.id.invio: {

                if(puCode.getText().length() == 0) {
                    puCode.setText("0");
                    Toast errMessage = Toast.makeText(this, "Errore! PU code non inserito", Toast.LENGTH_LONG);
                    errMessage.show();
                }

                Toast okMessage = Toast.makeText(this, "PU Code trovato! puoi ritornare alla pagina principale", Toast.LENGTH_SHORT);
                okMessage.show();

                backHome.setClickable(true);


                break;
            }

            case R.id.backHome: {

                //back to home page
                startActivity(new Intent(PUReader.this, MainActivity.class));

            }

        }



    }
}
