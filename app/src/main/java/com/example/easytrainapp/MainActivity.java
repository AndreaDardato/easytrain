package com.example.easytrainapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        //link to my PU button
        Button puButton = findViewById(R.id.PUReader);

        puButton.setOnClickListener(this);

        //link to my QRCodeButton
        Button qrCodeButton = findViewById(R.id.QRCode);



    }

    @Override
    public void onClick(View view) {

        switch(view.getId()) {

            case R.id.PUReader: {
                //chiamare un'altra finestra
                startActivity(new Intent(MainActivity.this, PUReader.class));

            }

        }

    }
}
